﻿using System;
using System.Threading.Tasks;
using ChannelEngine.Core.ChannelEngineServices.ProductService;
using ChannelEngine.Core.ChannelEngineServices.ProductService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;
using Microsoft.AspNetCore.Mvc;

namespace ChannelEngine.WebApp.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductFeaturesService _productFeaturesService;

        public ProductsController(IProductFeaturesService productFeaturesService)
        {
            _productFeaturesService = productFeaturesService;
        }

        // GET: Products
        public async Task<IActionResult> TopFiveSoldProductsDesc()
        {
            var topSoldProductsResult = await _productFeaturesService.GetTopFiveSoldInProgressProducts();
            if (topSoldProductsResult is SuccessResult<ProductsContent> && topSoldProductsResult.Data.Content != null)
                return View(topSoldProductsResult.Data.Content);

            throw new ApplicationException(
                $"{topSoldProductsResult.ResultType.ToString()} Reason: {topSoldProductsResult.Error}");
        }

        // POST: Products/Restock/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Restock(string id)
        {

            var restockResult = await _productFeaturesService.RestockProduct(id);
            if (restockResult is SuccessResult<object>)
                return RedirectToAction(nameof(TopFiveSoldProductsDesc));

            throw new ApplicationException($"{restockResult.ResultType.ToString()} Reason: {restockResult.Error}");
        }
    }
}