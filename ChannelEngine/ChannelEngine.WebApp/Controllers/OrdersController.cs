﻿using System;
using System.Threading.Tasks;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;
using Microsoft.AspNetCore.Mvc;

namespace ChannelEngine.WebApp.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IOrderFeaturesService _orderFeaturesService;

        public OrdersController(IOrderFeaturesService orderFeaturesService)
        {
            _orderFeaturesService = orderFeaturesService;
        }

        public async Task<IActionResult> OrdersInProgress()
        {
            var ordersResult =  await _orderFeaturesService.GetAllInProgress();
            if(ordersResult is SuccessResult<OrdersContent> && ordersResult.Data.Content != null)
                return View(ordersResult.Data.Content);

            throw new ApplicationException(
                $"{ordersResult.ResultType.ToString()} Reason: {ordersResult.Error}");
        }
    }
}