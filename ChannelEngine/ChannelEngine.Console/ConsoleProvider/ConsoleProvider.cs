﻿using System;

namespace ChannelEngine.ConsoleApp.ConsoleProvider
{
    internal interface IConsoleProvider
    {
        string ReadLine();
        void WriteLine(string text);
    }

    internal class ConsoleProvider : IConsoleProvider
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }

        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }
    }
}
