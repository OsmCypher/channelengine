﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChannelEngine.ConsoleApp.BusinessFeatureRunners;

namespace ChannelEngine.ConsoleApp
{
    internal class AppHost
    {
        private readonly IEnumerable<IBusinessFeatureRunner> _businessFeatureRunners;

        public AppHost(IEnumerable<IBusinessFeatureRunner> businessFeatureRunners)
        {
            _businessFeatureRunners = businessFeatureRunners;
        }

        public async Task Run()
        {
            foreach (var runner in _businessFeatureRunners)
            {
                await runner.RunFeature();
                Console.WriteLine("\n");
            }

            Console.ReadKey();
        }
    }
}
