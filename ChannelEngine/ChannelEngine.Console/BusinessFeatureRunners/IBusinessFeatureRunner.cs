﻿using System.Threading.Tasks;

namespace ChannelEngine.ConsoleApp.BusinessFeatureRunners
{
    internal interface IBusinessFeatureRunner
    {
        Task RunFeature();
    }
}