﻿using System.Threading.Tasks;
using ChannelEngine.ConsoleApp.ConsoleProvider;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;

namespace ChannelEngine.ConsoleApp.BusinessFeatureRunners
{
    internal class GetOrdersInProgress : IBusinessFeatureRunner
    {
        private readonly IOrderFeaturesService _orderFeaturesService;
        private readonly IConsoleProvider _consoleProvider;

        public GetOrdersInProgress(IOrderFeaturesService orderFeaturesService, IConsoleProvider consoleProvider)
        {
            _orderFeaturesService = orderFeaturesService;
            _consoleProvider = consoleProvider;
        }

        public async Task RunFeature()
        {
            _consoleProvider.WriteLine("Orders currently in progress:");

            var ordersResult = await _orderFeaturesService.GetAllInProgress();

            if (ordersResult is SuccessResult<OrdersContent>)
            {
                foreach (var order in ordersResult.Data.Content)
                {
                    _consoleProvider.WriteLine(order.ToString());
                }
            }
            else
            {
                _consoleProvider.WriteLine($"{ordersResult.ResultType} Reason: {ordersResult.Error}");
            }
        }
    }
}
