﻿using System.Threading.Tasks;
using ChannelEngine.ConsoleApp.ConsoleProvider;
using ChannelEngine.Core.ChannelEngineServices.ProductService;
using ChannelEngine.Core.ChannelEngineServices.Result;

namespace ChannelEngine.ConsoleApp.BusinessFeatureRunners
{
    internal class RestockProductToDefault : IBusinessFeatureRunner
    {
        private readonly IProductFeaturesService _productFeaturesService;
        private readonly IConsoleProvider _consoleProvider;

        public RestockProductToDefault(
            IProductFeaturesService productFeaturesService, 
            IConsoleProvider consoleProvider)
        {
            _productFeaturesService = productFeaturesService;
            _consoleProvider = consoleProvider;
        }

        public async Task RunFeature()
        {
            _consoleProvider.WriteLine("Provide merchant product number to restock it to 25");
            var merchantProductNo = _consoleProvider.ReadLine();
            var productRestockResult = await _productFeaturesService.RestockProduct(merchantProductNo);

            _consoleProvider.WriteLine(productRestockResult is SuccessResult<object>
                ? "Product has been restocked successfully"
                : $"{productRestockResult.ResultType} Reason: {productRestockResult.Error}");
        }
    }
}
