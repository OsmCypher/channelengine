﻿using System.Threading.Tasks;
using ChannelEngine.ConsoleApp.ConsoleProvider;
using ChannelEngine.Core.ChannelEngineServices.ProductService;
using ChannelEngine.Core.ChannelEngineServices.ProductService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;

namespace ChannelEngine.ConsoleApp.BusinessFeatureRunners
{
    internal class GetTopSoldProductsDescending : IBusinessFeatureRunner
    {
        private readonly IProductFeaturesService _productFeaturesService;
        private readonly IConsoleProvider _consoleProvider;

        public GetTopSoldProductsDescending(
            IProductFeaturesService productFeaturesService, 
            IConsoleProvider consoleProvider)
        {
            _productFeaturesService = productFeaturesService;
            _consoleProvider = consoleProvider;
        }
        public async Task RunFeature()
        {
            _consoleProvider.WriteLine("Top five sold products in descending order:");

            var productsResult = await _productFeaturesService.GetTopFiveSoldInProgressProducts();
            if (productsResult is SuccessResult<ProductsContent>)
            {
                foreach (var product in productsResult.Data.Content)
                {
                    _consoleProvider.WriteLine(product.ToString());
                }
            }
            else
            {
                _consoleProvider.WriteLine($"{productsResult.ResultType} Reason: {productsResult.Error}");
            }
        }
    }
}