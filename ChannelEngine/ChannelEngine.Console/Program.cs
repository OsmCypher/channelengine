﻿using System.IO;
using System.Threading.Tasks;
using ChannelEngine.ConsoleApp.BusinessFeatureRunners;
using ChannelEngine.ConsoleApp.ConsoleProvider;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.ProductService;
using ChannelEngine.Core.ChannelEngineServices.Result;
using ChannelEngine.Core.Infrastructure.ChannelEngine;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ChannelEngine.ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection, configuration);

            var serviceProvider = serviceCollection.BuildServiceProvider();
            var appHost = serviceProvider.GetService<AppHost>();
            await appHost.Run();
        }

        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient();
            services.Configure<ChannelEngineApiSettings>(configuration.GetSection("ChannelEngineApiSettings"));
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<ChannelEngineApiSettings>>().Value);
            services.AddHttpClient();
            services.AddSingleton<IOrderService, OrderService>();
            services.AddSingleton<IResultMapper, ResultMapper>();
            services.AddSingleton<IOrderFeaturesService, OrderFeaturesService>();
            services.AddSingleton<IProductService, ProductService>();
            services.AddSingleton<IConsoleProvider, ConsoleProvider.ConsoleProvider>();
            services.AddSingleton<IProductFeaturesService, ProductFeaturesService>();
            services.AddSingleton<IBusinessFeatureRunner, GetOrdersInProgress>();
            services.AddSingleton<IBusinessFeatureRunner, GetTopSoldProductsDescending>();
            services.AddSingleton<IBusinessFeatureRunner, RestockProductToDefault>();
            services.AddSingleton<AppHost>();
        }
    }
}
