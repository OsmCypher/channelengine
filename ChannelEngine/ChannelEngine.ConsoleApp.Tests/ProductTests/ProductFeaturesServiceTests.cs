﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using AutoFixture;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.ChannelEngineServices.ProductService;
using ChannelEngine.Core.ChannelEngineServices.ProductService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;
using FluentAssertions;
using Moq;
using Xunit;

namespace ChannelEngine.Core.Tests.ProductTests
{
    public class ProductFeaturesServiceTests : ProductFeaturesServiceTestsContext
    {
        [Fact]
        public async Task GivenNoOrdersInProgress_ItShouldReturnProperErrorResult()
        {
            var notFoundResult = new ErrorResult<OrdersContent>(ResultType.NotFound, "Resource has not been found");
            OrderFeaturesServiceMock
                .Setup(serviceMock => serviceMock.GetAllInProgress())
                .ReturnsAsync(notFoundResult);
            var sut = Fixture.Create<ProductFeaturesService>();

            var result = await sut.GetTopFiveSoldInProgressProducts();

            result.Should().BeOfType<ErrorResult<ProductsContent>>();
            result.ResultType.Should().BeEquivalentTo(ResultType.NotFound);
            result.Error.Should().BeEquivalentTo(ProductFeaturesServiceErrorMessages.NoOrdersInProgressFound);
        }

        [Theory]
        [MemberData(nameof(OrdersServiceErrorResponsesTestSet))]
        public async Task GivenErrorResponseFromOrdersService_ItShouldReturnProperErrorResult(Result<OrdersContent> errorResponse)
        {
            OrderFeaturesServiceMock
                .Setup(serviceMock => serviceMock.GetAllInProgress())
                .ReturnsAsync(errorResponse);
            var sut = Fixture.Create<ProductFeaturesService>();

            var result = await sut.GetTopFiveSoldInProgressProducts();

            result.Should().BeOfType<ErrorResult<ProductsContent>>();
            result.ResultType.Should().BeEquivalentTo(errorResponse.ResultType);
            result.Error.Should().BeEquivalentTo(errorResponse.Error);
        }

        [Fact]
        public async Task GivenOrdersReturned_ItShouldFetchTopFiveSoldProducts()
        {
            var returnedOrders = new SuccessResult<OrdersContent>(new OrdersContent
            {
                Content = new List<Order>
                {
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "1",
                                Description = "Product1",
                                Quantity = 1
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "2",
                                Description = "Product2",
                                Quantity = 3
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "1",
                                Description = "Product1",
                                Quantity = 1
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "3",
                                Description = "Product3",
                                Quantity = 2
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 6
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "5",
                                Description = "Product5",
                                Quantity = 5
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "6",
                                Description = "Product6",
                                Quantity = 8
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 6
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "7",
                                Description = "Product7",
                                Quantity = 4
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "8",
                                Description = "Product8",
                                Quantity = 6
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 1
                            }
                        }
                    },
                }
            });

            var expectedMerchantNumbers = new List<string>
            {
                "4",
                "6",
                "8",
                "5",
                "7",
            };

            OrderFeaturesServiceMock
                .Setup(serviceMock => serviceMock.GetAllInProgress())
                .ReturnsAsync(returnedOrders);

            var productsServiceMock = Fixture.Freeze<Mock<IProductService>>();
            productsServiceMock.Setup(
                service => service.GetProductsByMerchantProductNo(It.IsAny<IEnumerable<string>>()));

            var sut = Fixture.Create<ProductFeaturesService>();
            await sut.GetTopFiveSoldInProgressProducts();

            productsServiceMock
                .Verify(service =>
                    service.GetProductsByMerchantProductNo(expectedMerchantNumbers),
                    Times.Once);
        }

        [Fact]
        public async Task GivenFewTopOrdersExAequo_ItShouldOnlyFetchFive()
        {
            var returnedOrders = new SuccessResult<OrdersContent>(new OrdersContent
            {
                Content = new List<Order>
                {
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "1",
                                Description = "Product1",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "2",
                                Description = "Product2",
                                Quantity = 4
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "1",
                                Description = "Product1",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "3",
                                Description = "Product3",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 6
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "5",
                                Description = "Product5",
                                Quantity = 6
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "6",
                                Description = "Product6",
                                Quantity = 11
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 4
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "7",
                                Description = "Product7",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "8",
                                Description = "Product8",
                                Quantity = 4
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 1
                            }
                        }
                    },
                }
            });

            var merchantNumbersThatHasToBeInTopList = new List<string>
            {
                "6",
                "4",
                "5",
                "1"
            };

            var productIdsThatCanSupplementTopList = new List<string>
            {
                "8",
                "2"
            };

            OrderFeaturesServiceMock
                .Setup(serviceMock => serviceMock.GetAllInProgress())
                .ReturnsAsync(returnedOrders);

            var productsServiceMock = Fixture.Freeze<Mock<IProductService>>();
            productsServiceMock.Setup(
                service => service.GetProductsByMerchantProductNo(It.IsAny<IEnumerable<string>>()));

            var sut = Fixture.Create<ProductFeaturesService>();
            await sut.GetTopFiveSoldInProgressProducts();

            Expression<Func<List<string>, bool>> idsCollectionBeingPassToFetchAssertion = 
                (productIds) =>
                    productIds.Intersect(merchantNumbersThatHasToBeInTopList)
                        .SequenceEqual(merchantNumbersThatHasToBeInTopList)
                    && productIds.Except(merchantNumbersThatHasToBeInTopList)
                        .All(id => productIdsThatCanSupplementTopList.Contains(id))
                    && productIds.Count() == 5;

            productsServiceMock
                .Verify(service =>
                        service.GetProductsByMerchantProductNo(
                    It.Is(idsCollectionBeingPassToFetchAssertion)),
                    Times.Once);
        }

        [Fact]
        public async Task 
            GivenProductsFetchedFromProductsService_ItShouldReturnProductsWithProperQuantitiesDescending()
        {
            var returnedOrders = new SuccessResult<OrdersContent>(new OrdersContent
            {
                Content = new List<Order>
                {
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "1",
                                Description = "Product1",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "2",
                                Description = "Product2",
                                Quantity = 1
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "1",
                                Description = "Product1",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "3",
                                Description = "Product3",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 6
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "5",
                                Description = "Product5",
                                Quantity = 6
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "6",
                                Description = "Product6",
                                Quantity = 11
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 4
                            }
                        }
                    },
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "7",
                                Description = "Product7",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "8",
                                Description = "Product8",
                                Quantity = 4
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "4",
                                Description = "Product4",
                                Quantity = 1
                            }
                        }
                    },
                }
            });
            var merchantNumbersThatHasToBeInTopList = new List<string>
            {
                "6",
                "4",
                "5",
                "1",
                "8"
            };
            var fetchedProducts = new List<Product>
            {
                new Product
                {
                    MerchantProductNo = "4",
                },
                new Product
                {
                    MerchantProductNo = "5",
                },
                new Product
                {
                    MerchantProductNo = "6",
                },
                new Product
                {
                    MerchantProductNo = "1",
                },
                new Product
                {
                    MerchantProductNo = "8",
                }
            };

            var expectedProducts = new List<Product>
            {
                new Product
                {
                    MerchantProductNo = "6",
                    TotalQuantity = 11
                },
                new Product
                {
                    MerchantProductNo = "4",
                    TotalQuantity = 11
                },
                new Product
                {
                    MerchantProductNo = "5",
                    TotalQuantity = 6
                },
                new Product
                {
                    MerchantProductNo = "1",
                    TotalQuantity = 6
                },
                new Product
                {
                    MerchantProductNo = "8",
                    TotalQuantity = 4
                }
            };

            var fetchedProductsContent = new ProductsContent {Content = fetchedProducts};
            OrderFeaturesServiceMock
                .Setup(serviceMock => serviceMock.GetAllInProgress())
                .ReturnsAsync(returnedOrders);
            var responseContent = JsonContent.Create(fetchedProductsContent);
            var response = new HttpResponseMessage()
            {
                Content = responseContent,
                StatusCode = HttpStatusCode.OK
            };
            var productsServiceMock = Fixture.Freeze<Mock<IProductService>>();
            productsServiceMock.Setup(
                    service => service.GetProductsByMerchantProductNo(
                        It.Is<IEnumerable<string>>(productsIds =>
                            productsIds.SequenceEqual(merchantNumbersThatHasToBeInTopList))))
                .ReturnsAsync(response);
            var expectedResult = new SuccessResult<ProductsContent>(fetchedProductsContent);

            var mapperMock = Fixture.Freeze<Mock<IResultMapper>>();
            mapperMock.Setup(mapper => 
                mapper.MapResponseToResult<ProductsContent>(response)).ReturnsAsync(expectedResult);

            var sut = Fixture.Create<ProductFeaturesService>();
            var products = await sut.GetTopFiveSoldInProgressProducts();

            products.Should().BeOfType<SuccessResult<ProductsContent>>();
            products.Data.Content.Should().BeInDescendingOrder(product => product.TotalQuantity);
            products.Data.Content.Should().BeEquivalentTo(expectedProducts);
        }

        [Fact]
        public async Task GivenLessThenFiveProductsInOrders_ItShouldFetchThemAll()
        {
            var returnedOrders = new SuccessResult<OrdersContent>(new OrdersContent
            {
                Content = new List<Order>
                {
                    new Order
                    {
                        Lines = new List<OrderProduct>
                        {
                            new OrderProduct
                            {
                                MerchantProductNo = "1",
                                Description = "Product1",
                                Quantity = 3
                            },
                            new OrderProduct
                            {
                                MerchantProductNo = "2",
                                Description = "Product2",
                                Quantity = 1
                            }
                        }
                    }
                }
            });
            var expectedMerchantNumbers = new List<string>
            {
                "1",
                "2"
            };

            OrderFeaturesServiceMock
                .Setup(serviceMock => serviceMock.GetAllInProgress())
                .ReturnsAsync(returnedOrders);

            var productsServiceMock = Fixture.Freeze<Mock<IProductService>>();
            productsServiceMock.Setup(
                service => service.GetProductsByMerchantProductNo(It.IsAny<IEnumerable<string>>()));

            var sut = Fixture.Create<ProductFeaturesService>();
            await sut.GetTopFiveSoldInProgressProducts();

            productsServiceMock
                .Verify(service =>
                        service.GetProductsByMerchantProductNo(expectedMerchantNumbers),
                    Times.Once);
        }

        [Theory]
        [MemberData(nameof(MapperResultsTestSet))]
        public async Task 
            GivenProductsResultReturnedFromMapper_SameResultShouldBeReturned(Result<ProductsContent> expectedResult)
        {
            OrderFeaturesServiceMock
                .Setup(serviceMock =>
                    serviceMock
                        .GetAllInProgress()).ReturnsAsync(Fixture.Create<SuccessResult<OrdersContent>>());

            var mapperMock = Fixture.Freeze<Mock<IResultMapper>>();
            mapperMock
                .Setup(mapper => 
                    mapper.MapResponseToResult<ProductsContent>(It.IsAny<HttpResponseMessage>()))
                .ReturnsAsync(expectedResult);

            var sut = Fixture.Create<ProductFeaturesService>();
            var products = await sut.GetTopFiveSoldInProgressProducts();

            products.Should().BeOfType(expectedResult.GetType());
            products.Data.Should().Equals(expectedResult);
        }
    }
}
