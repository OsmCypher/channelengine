﻿using System.Collections.Generic;
using System.Net.Http;
using AutoFixture;
using AutoFixture.AutoMoq;
using ChannelEngine.Core.Infrastructure.ChannelEngine;
using Microsoft.Extensions.Options;
using Moq;

namespace ChannelEngine.Core.Tests.ProductTests
{
    public class ProductServiceTestsContext
    {
        protected static readonly string BaseTestUrl = "http://testbaseurl";
        protected static readonly string ProductsTestUrlPart = "products";
        protected static readonly string ApiTestKey = "testapikey";
        protected static readonly string CombinedBaseTestUrl =
            $"{BaseTestUrl}/{ProductsTestUrlPart}?apiKey={ApiTestKey}";
        protected static readonly ChannelEngineApiSettings ChannelEngineApiSettings =
            new ChannelEngineApiSettings()
            {
                BaseUrl = BaseTestUrl,
                ApiKey = ApiTestKey,
                Products = ProductsTestUrlPart
            };

        protected Fixture Fixture;
        protected Mock<IOptions<ChannelEngineApiSettings>> ApiSettingsMock;
        protected Mock<HttpMessageHandler> HttpMessageHandlerMock;
        protected Mock<IHttpClientFactory> HttpClientFactoryMock;

        protected ProductServiceTestsContext()
        {
            Fixture = new Fixture();
            Fixture.Customize(new AutoMoqCustomization());
            ApiSettingsMock = Fixture.Freeze<Mock<IOptions<ChannelEngineApiSettings>>>();
            HttpMessageHandlerMock = Fixture.Freeze<Mock<HttpMessageHandler>>();
            HttpClientFactoryMock = Fixture.Freeze<Mock<IHttpClientFactory>>();
        }

        protected void SetDefaultApiSettings()
        {
            ApiSettingsMock
                .Setup(options => options.Value)
                .Returns(ChannelEngineApiSettings);
        }

        protected void MockHttpClientFactory()
        {
            var httpClient = new HttpClient(HttpMessageHandlerMock.Object);
            HttpClientFactoryMock.Setup(httpClientFactory =>
                httpClientFactory.CreateClient(It.IsAny<string>())).Returns(httpClient);
        }

        public static IEnumerable<object[]> DataSetForGettingProducts()
        {
            yield return new object[]
            {
                new List<string> {"MMM-222"},
                $"{CombinedBaseTestUrl}&merchantProductNoLists=MMM-222"
            };
            yield return new object[]
            {
                new List<string>
                {
                   "MMM-222",
                   "TTT-222",
                   "ZZZ-222"
                },
                $"{CombinedBaseTestUrl}" +
                "&merchantProductNoLists=MMM-222" +
                "&merchantProductNoLists=TTT-222" +
                "&merchantProductNoLists=ZZZ-222"
            };
        }
    }
}
