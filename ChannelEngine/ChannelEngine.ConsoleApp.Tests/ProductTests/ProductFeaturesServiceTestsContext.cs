﻿using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.ChannelEngineServices.ProductService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;
using Moq;

namespace ChannelEngine.Core.Tests.ProductTests
{
    public class ProductFeaturesServiceTestsContext
    {
        protected Fixture Fixture;
        protected Mock<IOrderFeaturesService> OrderFeaturesServiceMock;

        protected ProductFeaturesServiceTestsContext()
        {
            Fixture = new Fixture();
            Fixture.Customize(new AutoMoqCustomization());
            OrderFeaturesServiceMock = Fixture.Freeze<Mock<IOrderFeaturesService>>();
        }

        public static IEnumerable<object[]> OrdersServiceErrorResponsesTestSet()
        {
            yield return new object[] {new ErrorResult<OrdersContent>(ResultType.Invalid, "Invalid request")};
            yield return new object[] { new ErrorResult<OrdersContent>(ResultType.PermissionDenied, "Permission denied") };
            yield return new object[] { new ErrorResult<OrdersContent>(ResultType.Unauthorized, "Unauthorized") };
            yield return new object[] { new ErrorResult<OrdersContent>(ResultType.Unexpected, "Unexpected") };
        }

        public static IEnumerable<object[]> MapperResultsTestSet()
        {
            yield return new object[] { new SuccessResult<ProductsContent>(new ProductsContent
                { Content = new Fixture().Create<IEnumerable<Product>>()}) };
            yield return new object[] {new ErrorResult<ProductsContent>(ResultType.Invalid, "Invalid request") };
            yield return new object[] { new ErrorResult<ProductsContent>(ResultType.PermissionDenied, "Permission denied") };
            yield return new object[] { new ErrorResult<ProductsContent>(ResultType.Unauthorized, "Unauthorized") };
            yield return new object[] { new ErrorResult<ProductsContent>(ResultType.Unexpected, "Unexpected") };
        }
    }
}
