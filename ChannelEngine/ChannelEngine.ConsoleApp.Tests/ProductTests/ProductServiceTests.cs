﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using ChannelEngine.Core.ChannelEngineServices.ProductService;
using ChannelEngine.Core.Infrastructure.ChannelEngine;
using FluentAssertions;
using Moq;
using Moq.Protected;
using Xunit;

namespace ChannelEngine.Core.Tests.ProductTests
{
    public class ProductServiceTests : ProductServiceTestsContext
    {
        [Fact]
        public void GivenApiSettingsNull_ItShouldThrowArgumentNullException()
        {
            ApiSettingsMock
                .Setup(options => options.Value)
                .Returns((ChannelEngineApiSettings)null);

            Action createService = () => new ProductService(HttpClientFactoryMock.Object, ApiSettingsMock.Object);

            createService.Should()
                .Throw<ArgumentNullException>()
                .WithMessage($"Value cannot be null.Parameter name: {nameof(ChannelEngineApiSettings)}");
        }

        [Theory]
        [MemberData(nameof(DataSetForGettingProducts))]
        public async Task GivenGetProductsByMerchantProductNoInvoked_ItShouldInvokeGetWithProperParameters(
            IEnumerable<string> requestedOrderStatuses,
            string expectedRequestUri)
        {
            SetDefaultApiSettings();
            HttpMessageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>());
            MockHttpClientFactory();

            var sut = Fixture.Create<ProductService>();

            await sut.GetProductsByMerchantProductNo(requestedOrderStatuses);

            HttpMessageHandlerMock
                .Protected()
                .Verify(
                    "SendAsync",
                    Times.Exactly(1),
                    ItExpr.Is<HttpRequestMessage>(request =>
                        request.Method == HttpMethod.Get && request.RequestUri.ToString() == expectedRequestUri),
                    ItExpr.IsAny<CancellationToken>());
        }

        [Fact]
        public void GivenNullMerchantProductNumbersProvided_ItShouldThrowArgumentNullExceptionWithProperMessage()
        {
            SetDefaultApiSettings();
            MockHttpClientFactory();

            var sut = Fixture.Create<ProductService>();

            Func<Task> action = async () => await sut.GetProductsByMerchantProductNo(null);
            action
                .Should()
                .ThrowExactly<ArgumentNullException>()
                .WithMessage($"Value cannot be null.Parameter name: merchantProductNumbers");
        }

        [Fact]
        public void GivenEmptyMerchantProductNumbersProvided_ItShouldThrowArgumentExceptionWithProperMessage()
        {
            SetDefaultApiSettings();
            MockHttpClientFactory();

            var sut = Fixture.Create<ProductService>();

            Func<Task> action = async () => await sut.GetProductsByMerchantProductNo(new List<string>());
            action
                .Should()
                .ThrowExactly<ArgumentException>()
                .WithMessage(ProductServiceErrorMessages.CollectionOfMerchantProductNumbersIsEmpty);
        }

        [Fact]
        public async Task GivenResponseReturnedFromApi_ItShouldReturnSameResponse()
        {
            SetDefaultApiSettings();

            var errorReason = "Resource has not been found";

            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NotFound,
                ReasonPhrase = errorReason
            };

            HttpMessageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(response);
            MockHttpClientFactory();
            var sut = Fixture.Create<ProductService>();

            var result = await sut.GetProductsByMerchantProductNo(new List<string> { "MMM-222" });

            result.Should().BeEquivalentTo(response);
        }
    }
}
