﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using AutoFixture;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;
using FluentAssertions;
using Moq;
using Xunit;

namespace ChannelEngine.Core.Tests.OrderTests
{
    public class OrderFeatureServiceTests : OrderFeaturesServiceTestsContext
    {
        [Fact]
        public async Task GivenAllInProgressInvoked_ThenItShouldInvokeOrderServiceWithProperParameter()
        {
            await Sut.GetAllInProgress();

            OrderServiceMock.Verify(service => service.GetOrdersByStatus(
                new List<OrderStatuses> {OrderStatuses.InProgress}),
                Times.Once);
        }

        [Fact]
        public async Task GivenAllInProgressInvoked_ThenItShouldReturnAllOrdersInProgress()
        {
            Fixture.Customize<Order>(customize => customize.With(order => order.Status, OrderStatuses.InProgress));
            var expectedOrders = new OrdersContent
            {
                Content = Fixture.CreateMany<Order>()
            };
            var responseContent = JsonContent.Create(expectedOrders);
            var response = new HttpResponseMessage()
            {
                Content = responseContent,
                StatusCode = HttpStatusCode.OK
            };
            OrderServiceMock
                .Setup(orderService => 
                    orderService.GetOrdersByStatus(new List<OrderStatuses> { OrderStatuses.InProgress }))
                .ReturnsAsync(response);
            var expectedResult = new SuccessResult<OrdersContent>(expectedOrders);
            MapperMock.Setup(mapper => 
                mapper.MapResponseToResult<OrdersContent>(response)).ReturnsAsync(expectedResult);
            var sut = Fixture.Create<OrderFeaturesService>();

            var orders = await sut.GetAllInProgress();

            orders.Should().BeEquivalentTo(expectedResult);
            orders.Data.Should().BeEquivalentTo(expectedOrders);
            orders
                .Data
                .Content
                .Select(order => order.Status)
                .Should()
                .AllBeEquivalentTo(OrderStatuses.InProgress);
        }

        [Fact]
        public async Task GivenOrderServiceThrowsException_ThenItShouldBeHandledAsResult()
        {
            var exceptionMessage = "Some unexpected error occured";
            var exceptionToBeThrown = new Exception(exceptionMessage);
            OrderServiceMock
                .Setup(orderService => 
                    orderService
                        .GetOrdersByStatus(new List<OrderStatuses> { OrderStatuses.InProgress }))
                        .ThrowsAsync(exceptionToBeThrown);

            var result = await Sut.GetAllInProgress();

            result.Should().BeOfType<ErrorResult<OrdersContent>>();
            result.ResultType.Should().BeEquivalentTo(ResultType.Unexpected);
            result.Error.Should().BeEquivalentTo(exceptionMessage);
        }
    }
}
