﻿using System.Collections.Generic;
using System.Net.Http;
using AutoFixture;
using AutoFixture.AutoMoq;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.Infrastructure.ChannelEngine;
using ChannelEngine.Core.Utils;
using Microsoft.Extensions.Options;
using Moq;

namespace ChannelEngine.Core.Tests.OrderTests
{
    public abstract class OrderServiceTextsContext
    {
        protected static readonly string BaseTestUrl = "http://testbaseurl";
        protected static readonly string OrdersTestUrlPart = "orders";
        protected static readonly string ApiTestKey = "testapikey";
        protected static readonly string CombinedBaseTestUrl = 
            $"{BaseTestUrl}/{OrdersTestUrlPart}?apiKey={ApiTestKey}";
        protected static readonly ChannelEngineApiSettings ChannelEngineApiSettings =
            new ChannelEngineApiSettings()
            {
                BaseUrl = BaseTestUrl,
                ApiKey = ApiTestKey,
                Orders = OrdersTestUrlPart
            };

        protected Fixture Fixture;
        protected Mock<IOptions<ChannelEngineApiSettings>> ApiSettingsMock;
        protected Mock<HttpMessageHandler> HttpMessageHandlerMock;
        protected Mock<IHttpClientFactory> HttpClientFactoryMock;

        protected OrderServiceTextsContext()
        {
            Fixture = new Fixture();
            Fixture.Customize(new AutoMoqCustomization());
            ApiSettingsMock = Fixture.Freeze<Mock<IOptions<ChannelEngineApiSettings>>>();
            HttpMessageHandlerMock = Fixture.Freeze<Mock<HttpMessageHandler>>();
            HttpClientFactoryMock = Fixture.Freeze<Mock<IHttpClientFactory>>();
        }

        protected void SetDefaultApiSettings()
        {
            ApiSettingsMock
                .Setup(options => options.Value)
                .Returns(ChannelEngineApiSettings);
        }

        protected void MockHttpClientFactory()
        {
            var httpClient = new HttpClient(HttpMessageHandlerMock.Object);
            HttpClientFactoryMock.Setup(httpClientFactory =>
                httpClientFactory.CreateClient(It.IsAny<string>())).Returns(httpClient);
        }

        public static IEnumerable<object[]> DataSetForGettingOrdersByStatuses()
        {
            yield return new object[]
            {
                new List<OrderStatuses> {OrderStatuses.InProgress},
                $"{CombinedBaseTestUrl}&statuses={OrderStatuses.InProgress.GetEnumValue()}"
            };
            yield return new object[]
            {
                new List<OrderStatuses>
                {
                    OrderStatuses.InProgress,
                    OrderStatuses.Canceled,
                    OrderStatuses.InCombi
                },
                $"{CombinedBaseTestUrl}" +
                $"&statuses={OrderStatuses.InProgress.GetEnumValue()}" +
                $"&statuses={OrderStatuses.Canceled.GetEnumValue()}" +
                $"&statuses={OrderStatuses.InCombi.GetEnumValue()}"
            };
            yield return new object[]
            {
                new List<OrderStatuses>
                {
                    OrderStatuses.New,
                    OrderStatuses.Returned,
                    OrderStatuses.Shipped,
                    OrderStatuses.Manco,
                    OrderStatuses.RequiresCorrection
                },
                $"{CombinedBaseTestUrl}" +
                $"&statuses={OrderStatuses.New.GetEnumValue()}" +
                $"&statuses={OrderStatuses.Returned.GetEnumValue()}" +
                $"&statuses={OrderStatuses.Shipped.GetEnumValue()}" +
                $"&statuses={OrderStatuses.Manco.GetEnumValue()}" +
                $"&statuses={OrderStatuses.RequiresCorrection.GetEnumValue()}"
            };
        }
    }
}
