﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using AutoFixture;
using System.Threading.Tasks;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.Infrastructure.ChannelEngine;
using FluentAssertions;
using Moq;
using Moq.Protected;
using Xunit;

namespace ChannelEngine.Core.Tests.OrderTests
{
    public class OrderServiceTests : OrderServiceTextsContext
    {
        [Fact]
        public void GivenApiSettingsNull_ItShouldThrowArgumentNullException()
        {
            ApiSettingsMock
                .Setup(options => options.Value)
                .Returns((ChannelEngineApiSettings)null);

            Action createService = () => new OrderService(HttpClientFactoryMock.Object, ApiSettingsMock.Object);

            createService.Should()
                .Throw<ArgumentNullException>()
                .WithMessage($"Value cannot be null.Parameter name: {nameof(ChannelEngineApiSettings)}");
        }

        [Theory]
        [MemberData(nameof(DataSetForGettingOrdersByStatuses))]
        public async Task GivenGetOrdersByStatusInvoked_ItShouldInvokeGetWithProperParameters(
            IEnumerable<OrderStatuses> requestedOrderStatuses,
            string expectedRequestUri)
        {
            SetDefaultApiSettings();
            HttpMessageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync", 
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>());
            MockHttpClientFactory();
            var sut = Fixture.Create<OrderService>();

            await sut.GetOrdersByStatus(requestedOrderStatuses);

            HttpMessageHandlerMock
                .Protected()
                .Verify(
                    "SendAsync",
                    Times.Exactly(1),
                    ItExpr.Is<HttpRequestMessage>(request => 
                        request.Method == HttpMethod.Get && request.RequestUri.ToString() == expectedRequestUri),
                    ItExpr.IsAny<CancellationToken>());
        }

        [Fact]
        public void GivenNullStatusesProvided_ItShouldThrowArgumentNullExceptionWithProperMessage()
        {
            SetDefaultApiSettings();
            MockHttpClientFactory();

            var sut = Fixture.Create<OrderService>();

            Func<Task> action =  async () => await sut.GetOrdersByStatus(null);
            action
                .Should()
                .ThrowExactly<ArgumentNullException>()
                .WithMessage($"Value cannot be null.Parameter name: orderStatuses");
        }

        [Fact]
        public void GivenEmptyStatusesProvided_ItShouldThrowArgumentExceptionWithProperMessage()
        {
            SetDefaultApiSettings();
            MockHttpClientFactory();

            var sut = Fixture.Create<OrderService>();

            Func<Task> action = async () => await sut.GetOrdersByStatus(new List<OrderStatuses>());
            action
                .Should()
                .ThrowExactly<ArgumentException>()
                .WithMessage(OrderServiceErrorMessages.CollectionOfStatusesIsEmpty);
        }

        [Fact]
        public async Task GivenResponseReturnedFromApi_ItShouldReturnSameResponse()
        {
            SetDefaultApiSettings();

            var errorReason = "Resource has not been found";

            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NotFound,
                ReasonPhrase = errorReason
            };

            HttpMessageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(response);
            MockHttpClientFactory();
            var sut = Fixture.Create<OrderService>();

            var result = await sut.GetOrdersByStatus(new List<OrderStatuses> { OrderStatuses.InProgress });

            result.Should().BeEquivalentTo(response);
        }
    }
}