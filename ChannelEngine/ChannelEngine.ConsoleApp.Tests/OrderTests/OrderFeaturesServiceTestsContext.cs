﻿using AutoFixture;
using AutoFixture.AutoMoq;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.Result;
using Moq;

namespace ChannelEngine.Core.Tests.OrderTests
{
    public abstract class OrderFeaturesServiceTestsContext
    {
        protected Fixture Fixture;
        protected Mock<IOrderService> OrderServiceMock;
        protected OrderFeaturesService Sut;
        protected Mock<IResultMapper> MapperMock;

        protected OrderFeaturesServiceTestsContext()
        {
            Fixture = new Fixture();
            Fixture.Customize(new AutoMoqCustomization());
            OrderServiceMock = Fixture.Freeze<Mock<IOrderService>>();
            MapperMock = Fixture.Freeze<Mock<IResultMapper>>();

            Sut = Fixture.Create<OrderFeaturesService>();
        }
    }
}
