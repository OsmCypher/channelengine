﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ChannelEngine.Core.ChannelEngineServices.Result;
using FluentAssertions;
using Xunit;

namespace ChannelEngine.Core.Tests
{
    public class TestType
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
    public class ResultMapperTests : ResultMapperTestsContext
    {
        [Fact]
        public async Task GivenSuccessfulResponseCode_ItShouldReturnProperResult()
        {
            var response = new HttpResponseMessage()
            {
                Content = Content,
                StatusCode = HttpStatusCode.OK
            };
            var sut = new ResultMapper();

            var result = await sut.MapResponseToResult<TestType>(response);

            result.Should().BeOfType<SuccessResult<TestType>>();
            result.Data.Should().BeEquivalentTo(ExpectedData);
            result.Error.Should().BeEmpty();
        }

        [Theory]
        [MemberData(nameof(UnsuccessfulResponsesTestSet))]
        public async Task GivenUnsuccessfulResponse_ItShouldReturnProperErrorResult(
            HttpStatusCode httpStatusCode,
            ResultType resultType,
            string reasonPhase)
        {
            var response = new HttpResponseMessage()
            {
                Content = Content,
                StatusCode = httpStatusCode,
                ReasonPhrase = reasonPhase
            };
            var sut = new ResultMapper();

            var result = await sut.MapResponseToResult<TestType>(response);

            result.Should().BeOfType<ErrorResult<TestType>>();
            result.Error.Should().Be(reasonPhase);
            result.Data.Should().Be(null);
            result.ResultType.Should().Be(resultType);
        }

        [Fact]
        public void GivenNullResponseProvided_ItShouldThrowArgumentNullException()
        {
            var sut = new ResultMapper();

            Func<Task> action = async () => await sut.MapResponseToResult<TestType>(null);

            action
                .Should()
                .ThrowExactly<ArgumentNullException>()
                .WithMessage($"Value cannot be null.Parameter name: responseMessage");
        }
    }
}
