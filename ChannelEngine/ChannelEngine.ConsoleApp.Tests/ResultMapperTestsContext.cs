﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http.Json;
using ChannelEngine.Core.ChannelEngineServices.Result;

namespace ChannelEngine.Core.Tests
{
    public abstract class ResultMapperTestsContext
    {
        protected static TestType ExpectedData = new TestType()
        {
            Id = 1,
            Title = "Title"
        };
        protected static JsonContent Content = JsonContent.Create(ExpectedData);

        public static IEnumerable<object[]> UnsuccessfulResponsesTestSet()
        {
            yield return new object[]
            {
                HttpStatusCode.NotFound,
                ResultType.NotFound,
                "Cannot find resource"
            };
            yield return new object[]
            {
                HttpStatusCode.BadRequest,
                ResultType.Invalid,
                "Invalid request"
            };
            yield return new object[]
            {
                HttpStatusCode.Forbidden,
                ResultType.PermissionDenied,
                "Permission Denied"
            };
            yield return new object[]
            {
                HttpStatusCode.Unauthorized,
                ResultType.Unauthorized,
                "Unauthorized to perform this operation"
            };
            yield return new object[]
            {
                HttpStatusCode.GatewayTimeout,
                ResultType.Unexpected,
                "Gateway has timed out"
            };
            yield return new object[]
            {
                HttpStatusCode.InternalServerError,
                ResultType.Unexpected,
                "Internal server error occured"
            };
        }
    }
}
