﻿using System;

namespace ChannelEngine.Core.ChannelEngineServices.ProductService.Models
{
    internal class ProductOrderQuantity
    {
        public string Id { get; }
        public string Description { get; }
        public int TotalQuantity { get; set; }

        public ProductOrderQuantity(string id, string description)
        {
            Id = id;
            Description = description;
        }

        public override bool Equals(object obj)
        {
            return obj is ProductOrderQuantity quantity &&
                   Id == quantity.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }
    }
}
