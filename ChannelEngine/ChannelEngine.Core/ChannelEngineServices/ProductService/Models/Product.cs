﻿namespace ChannelEngine.Core.ChannelEngineServices.ProductService.Models
{
    public class Product
    {
        public string MerchantProductNo { get; set; }
        public string Name { get; set; }
        public string EAN { get; set; }
        public int TotalQuantity { get; set; }
        public int Stock { get; set; }

        public override string ToString()
        {
            return
                $"MerchantProductNo: {MerchantProductNo}, Name: {Name}, EAN: {EAN}, " +
                $"TotalQuantity: {TotalQuantity}, Stock: {Stock}";
        }
    }
}
