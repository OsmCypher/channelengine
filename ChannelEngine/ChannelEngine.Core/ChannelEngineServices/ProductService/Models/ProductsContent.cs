﻿using System.Collections.Generic;

namespace ChannelEngine.Core.ChannelEngineServices.ProductService.Models
{
    public class ProductsContent
    {
        public IEnumerable<Product> Content { get; set; }
    }
}
