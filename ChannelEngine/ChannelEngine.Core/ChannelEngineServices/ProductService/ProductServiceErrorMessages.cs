﻿namespace ChannelEngine.Core.ChannelEngineServices.ProductService
{
    public class ProductServiceErrorMessages
    {
        public static readonly string CollectionOfMerchantProductNumbersIsEmpty = 
            "Passed collection of merchant product numbers cannot be empty";
        public static readonly string MerchantProductNumbersNullOrWhiteSpace = "Merchant product number cannot be empty";
    }
}
