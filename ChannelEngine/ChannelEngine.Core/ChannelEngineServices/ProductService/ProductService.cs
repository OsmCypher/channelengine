﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ChannelEngine.Core.Infrastructure.ChannelEngine;
using Microsoft.Extensions.Options;

namespace ChannelEngine.Core.ChannelEngineServices.ProductService
{ 
    public interface IProductService
    {
        Task<HttpResponseMessage> GetProductsByMerchantProductNo(IEnumerable<string> merchantProductNumbers);
        Task<HttpResponseMessage> RestockProduct(string merchantProductNumber, int quantity);
    }

    public class ProductService : IProductService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ChannelEngineApiSettings _apiSettings;

        public ProductService(IHttpClientFactory httpClientFactory, IOptions<ChannelEngineApiSettings> apiSettingsOptions)
        {
            _httpClientFactory = httpClientFactory;
            _apiSettings = apiSettingsOptions.Value ??
                throw new ArgumentNullException(nameof(ChannelEngineApiSettings));
        }

        public async Task<HttpResponseMessage> GetProductsByMerchantProductNo(IEnumerable<string> merchantProductNumbers)
        {
            if (merchantProductNumbers is null) throw new ArgumentNullException(nameof(merchantProductNumbers));
            if (!merchantProductNumbers.Any())
                throw new ArgumentException(ProductServiceErrorMessages.CollectionOfMerchantProductNumbersIsEmpty);

            var httpClient = _httpClientFactory.CreateClient();

            var response = await httpClient.GetAsync(BuildGetRequestUrl(merchantProductNumbers));

            return response;
        }

        public async Task<HttpResponseMessage> RestockProduct(string merchantProductNumber, int quantity)
        {
            if (string.IsNullOrWhiteSpace(merchantProductNumber))
                throw new ArgumentException(
                    ProductServiceErrorMessages.MerchantProductNumbersNullOrWhiteSpace);
            
            var httpClient = _httpClientFactory.CreateClient();
            var url = 
                $"{_apiSettings.BaseUrl}/{_apiSettings.Products}/{merchantProductNumber}?apiKey={_apiSettings.ApiKey}";
            var patchString = $"[{{'op': 'replace', 'path': 'Stock', 'value': '{quantity.ToString()}'}}]";
            var content = new StringContent(patchString);

            return await httpClient.PatchAsync(url, content);
        }

        private string BuildGetRequestUrl(IEnumerable<string> merchantProductNumbers)
        {
            var baseUrl = $"{_apiSettings.BaseUrl}/{_apiSettings.Products}?apiKey={_apiSettings.ApiKey}";

            var stringBuilder = new StringBuilder(baseUrl);
            foreach (var merchantProductNumber in merchantProductNumbers)
            {
                stringBuilder.AppendFormat("&merchantProductNoLists={0}", merchantProductNumber);
            }

            return stringBuilder.ToString();
        }
    }

}
