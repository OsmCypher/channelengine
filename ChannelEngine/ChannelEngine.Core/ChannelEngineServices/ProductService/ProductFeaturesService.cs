﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChannelEngine.Core.ChannelEngineServices.OrderService;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.ChannelEngineServices.ProductService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;

namespace ChannelEngine.Core.ChannelEngineServices.ProductService
{
    public interface IProductFeaturesService
    {
        Task<Result<ProductsContent>> GetTopFiveSoldInProgressProducts();
        Task<Result<object>> RestockProduct(string merchantProductNumber);
    }

    public class ProductFeaturesService : IProductFeaturesService
    {
        private static readonly int _defaultRestockValue = 25;
        private readonly IOrderFeaturesService _ordersFeaturesService;
        private readonly IProductService _productService;
        private readonly IResultMapper _mapper;

        public ProductFeaturesService(
            IOrderFeaturesService ordersFeaturesService,
            IProductService productService,
            IResultMapper mapper)
        {
            _ordersFeaturesService = ordersFeaturesService;
            _productService = productService;
            _mapper = mapper;
        }

        public async Task<Result<ProductsContent>> GetTopFiveSoldInProgressProducts()
        {
            var ordersInProgressResult = await _ordersFeaturesService.GetAllInProgress();
            if(ordersInProgressResult.ResultType == ResultType.NotFound)
                return new ErrorResult<ProductsContent>(
                    ResultType.NotFound, 
                    ProductFeaturesServiceErrorMessages.NoOrdersInProgressFound);

            if(ordersInProgressResult is ErrorResult<OrdersContent>)
                return new ErrorResult<ProductsContent>(
                    ordersInProgressResult.ResultType,
                    ordersInProgressResult.Error);

            var topFiveProductsProducts = ComputeTopFiveProducts(ordersInProgressResult.Data.Content).ToList();
            var productsResponse = await _productService.GetProductsByMerchantProductNo(
                topFiveProductsProducts.Select(product => product.Id).ToList());

            var productsResult = await _mapper.MapResponseToResult<ProductsContent>(productsResponse);

            if (!(productsResult is SuccessResult<ProductsContent>)) return productsResult;
            
            foreach (var product in productsResult.Data.Content)
            {
                product.TotalQuantity = topFiveProductsProducts
                    .Where(topProduct => topProduct.Id == product.MerchantProductNo)
                    .Select(topProduct => topProduct.TotalQuantity).FirstOrDefault();
            }

            productsResult.Data.Content =
                productsResult.Data.Content
                    .Where(product => product.TotalQuantity > 0)
                    .OrderByDescending(product => product.TotalQuantity);

            return productsResult;
        }

        public async Task<Result<object>> RestockProduct(string merchantProductNumber)
        {
            if(string.IsNullOrWhiteSpace(merchantProductNumber))
                throw new ArgumentException(
                    ProductFeaturesServiceErrorMessages.MerchantProductNumbersNullOrWhiteSpace);

            var response = await _productService.RestockProduct(merchantProductNumber, _defaultRestockValue);
            var responseResult = await _mapper.MapResponseToResult<object>(response);

            return responseResult;
        }

        private IEnumerable<ProductOrderQuantity> ComputeTopFiveProducts(IEnumerable<Order> orders)
        {
            var flattenOrderedProductsList = orders
                .Select(order => order.Lines)
                .SelectMany(lines => lines);

            var productsAndQuantitiesPairs = CountTotalQuantityForEachProduct(flattenOrderedProductsList);
            var sortedQuantityGroupsOfProducts = GroupProductsByTotalQuantity(productsAndQuantitiesPairs);

            var topFiveProductsIds =
                sortedQuantityGroupsOfProducts
                    .Values
                    .SelectMany(orderProduct => orderProduct)
                    .TakeLast(5)
                    .Reverse();

            return topFiveProductsIds;
        }

        private Dictionary<ProductOrderQuantity, int> CountTotalQuantityForEachProduct(
            IEnumerable<OrderProduct> orderedProducts)
        {
            var eachProductQuantity = new Dictionary<ProductOrderQuantity, int>();

            foreach (var orderProduct in orderedProducts)
            {
                var productOrderQuantity = new ProductOrderQuantity(
                    orderProduct.MerchantProductNo,
                    orderProduct.Description);

                if (eachProductQuantity.TryAdd(productOrderQuantity, orderProduct.Quantity)) continue;

                eachProductQuantity[productOrderQuantity] += orderProduct.Quantity;
            }

            return eachProductQuantity;
        }

        private SortedDictionary<int, List<ProductOrderQuantity>> GroupProductsByTotalQuantity(
            Dictionary<ProductOrderQuantity, int> countedProducts)
        {
            var sortedGroupsOfProductsByQuantity = new SortedDictionary<int, List<ProductOrderQuantity>>();
            foreach (var productQuantityPair in countedProducts)
            {
                productQuantityPair.Key.TotalQuantity = productQuantityPair.Value;
                var beenAdded =
                    sortedGroupsOfProductsByQuantity.TryAdd(
                        productQuantityPair.Value,
                        new List<ProductOrderQuantity> {productQuantityPair.Key});

                if (!beenAdded)
                    sortedGroupsOfProductsByQuantity[productQuantityPair.Value].Add(productQuantityPair.Key);
            }

            return sortedGroupsOfProductsByQuantity;
        }
    }
}
