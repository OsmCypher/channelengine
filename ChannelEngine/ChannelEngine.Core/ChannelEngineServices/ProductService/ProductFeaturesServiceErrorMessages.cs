﻿namespace ChannelEngine.Core.ChannelEngineServices.ProductService
{
    public class ProductFeaturesServiceErrorMessages
    {
        public static readonly string NoOrdersInProgressFound = "No orders in progress have been found";
        public static readonly string MerchantProductNumbersNullOrWhiteSpace = "Merchant product number cannot be empty";
    }
}
