﻿namespace ChannelEngine.Core.ChannelEngineServices.Result
{
    public abstract class Result<T>
    {
        public abstract ResultType ResultType { get; }
        public abstract T Data { get; }
        public abstract string Error { get; }
    }

    public sealed class SuccessResult<T> : Result<T>
    {
        public override ResultType ResultType => ResultType.Ok;
        public override T Data { get; }
        public override string Error => string.Empty;

        public SuccessResult(T data)
        {
            Data = data;
        }
    }

    public sealed class ErrorResult<T> : Result<T>
    {
        public override ResultType ResultType { get; }
        public override T Data => default(T);
        public override string Error { get; }

        public ErrorResult(ResultType resultType, string error)
        {
            ResultType = resultType;
            Error = error;
        }
    }
}
