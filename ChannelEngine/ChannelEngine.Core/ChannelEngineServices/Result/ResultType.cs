﻿namespace ChannelEngine.Core.ChannelEngineServices.Result
{
    public enum ResultType
    {
        Ok,
        Unauthorized,
        Unexpected,
        PermissionDenied,
        Invalid,
        NotFound
    }
}
