﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChannelEngine.Core.ChannelEngineServices.Result
{
    public interface IResultMapper
    {
        Task<Result<T>> MapResponseToResult<T>(HttpResponseMessage responseMessage);
    }

    public class ResultMapper : IResultMapper
    {
        public async Task<Result<T>> MapResponseToResult<T>(HttpResponseMessage responseMessage)
        {
            if (responseMessage is null) throw new ArgumentNullException(nameof(responseMessage));

            if (!responseMessage.IsSuccessStatusCode)
                return new ErrorResult<T>(
                    MapErrorHttpStatusCodeToResultType(responseMessage.StatusCode),
                    responseMessage.ReasonPhrase);

            var content = await responseMessage.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<T>(content);
            return new SuccessResult<T>(data);
        }

        private ResultType MapErrorHttpStatusCodeToResultType(HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.BadRequest:
                    return ResultType.Invalid;
                case HttpStatusCode.Forbidden:
                    return ResultType.PermissionDenied;
                case HttpStatusCode.Unauthorized:
                    return ResultType.Unauthorized;
                case HttpStatusCode.NotFound:
                    return ResultType.NotFound;
                default:
                    return ResultType.Unexpected;
            }
        }
    }
}
