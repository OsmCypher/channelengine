﻿namespace ChannelEngine.Core.ChannelEngineServices.OrderService.Models
{
    public class OrderProduct
    {
        public string MerchantProductNo { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
    }
}
