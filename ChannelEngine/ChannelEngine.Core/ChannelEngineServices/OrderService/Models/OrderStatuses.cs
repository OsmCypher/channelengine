﻿using System.Runtime.Serialization;

namespace ChannelEngine.Core.ChannelEngineServices.OrderService.Models
{
    public enum OrderStatuses
    {
        [EnumMember(Value = "IN_PROGRESS")]
        InProgress,
        [EnumMember(Value = "SHIPPED")]
        Shipped,
        [EnumMember(Value = "IN_BACKORDER")]
        InBackOrder,
        [EnumMember(Value = "MANCO")]
        Manco,
        [EnumMember(Value = "CANCELLED")]
        Canceled,
        [EnumMember(Value = "IN_COMBI")]
        InCombi,
        [EnumMember(Value = "CLOSED")]
        Closed,
        [EnumMember(Value = "NEW")]
        New,
        [EnumMember(Value = "RETURNED")]
        Returned,
        [EnumMember(Value = "REQUIRES_CORRECTION")]
        RequiresCorrection
    }
}
