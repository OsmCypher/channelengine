﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ChannelEngine.Core.ChannelEngineServices.OrderService.Models
{
    public class Order
    {
        public int Id { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public OrderStatuses Status { get; set; }
        public bool IsBusinessOrder { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public IEnumerable<OrderProduct> Lines { get; set; }

        public override string ToString()
        {
            return
                $"Id: {Id}, Status: {Status}, IsBusinessOrder: {IsBusinessOrder}, " +
                $"CreatedAt: {CreatedAt.ToString("f")}, UpdatedAt: {UpdatedAt.ToString("f")}";
        }
    }
}
