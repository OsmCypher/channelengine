﻿using System.Collections.Generic;

namespace ChannelEngine.Core.ChannelEngineServices.OrderService.Models
{
    public class OrdersContent
    {
        public IEnumerable<Order> Content { get; set; }
    }
}
