﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.ChannelEngineServices.Result;

namespace ChannelEngine.Core.ChannelEngineServices.OrderService
{
    public interface IOrderFeaturesService
    {
        Task<Result<OrdersContent>> GetAllInProgress();
    }

    public class OrderFeaturesService : IOrderFeaturesService
    {
        private readonly IOrderService _orderService;
        private readonly IResultMapper _mapper;

        private static readonly IEnumerable<OrderStatuses> OrderStatus = 
            new List<OrderStatuses> { OrderStatuses.InProgress };

        public OrderFeaturesService(IOrderService orderService, IResultMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }

        public async Task<Result<OrdersContent>> GetAllInProgress()
        {
            try
            {
                var response = await _orderService.GetOrdersByStatus(OrderStatus);
                return await _mapper.MapResponseToResult<OrdersContent>(response);
            }
            catch (Exception e)
            {
                //should be also logged
                return new ErrorResult<OrdersContent>(ResultType.Unexpected, e.Message);
            }
        }
    }
}
