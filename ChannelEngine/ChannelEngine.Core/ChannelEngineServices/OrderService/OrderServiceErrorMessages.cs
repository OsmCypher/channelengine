﻿namespace ChannelEngine.Core.ChannelEngineServices.OrderService
{
    public class OrderServiceErrorMessages
    {
        public static readonly string CollectionOfStatusesIsEmpty = "Passed collection of statuses cannot be empty";
    }
}
