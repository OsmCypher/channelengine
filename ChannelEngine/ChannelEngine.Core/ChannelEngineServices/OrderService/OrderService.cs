﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ChannelEngine.Core.ChannelEngineServices.OrderService.Models;
using ChannelEngine.Core.Infrastructure.ChannelEngine;
using ChannelEngine.Core.Utils;
using Microsoft.Extensions.Options;

namespace ChannelEngine.Core.ChannelEngineServices.OrderService
{
    public interface IOrderService
    {
        Task<HttpResponseMessage> GetOrdersByStatus(IEnumerable<OrderStatuses> orderStatuses);
    }

    public class OrderService : IOrderService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ChannelEngineApiSettings _apiSettings;

        public OrderService(
            IHttpClientFactory clientFactory,
            IOptions<ChannelEngineApiSettings> apiSettingsOptions)
        {
            _clientFactory = clientFactory;
            _apiSettings = apiSettingsOptions.Value ?? 
                           throw new ArgumentNullException(nameof(ChannelEngineApiSettings));
        }

        public async Task<HttpResponseMessage> GetOrdersByStatus(IEnumerable<OrderStatuses> orderStatuses)
        {
            if (orderStatuses is null) throw new ArgumentNullException(nameof(orderStatuses));
            if (!orderStatuses.Any())
                throw new ArgumentException(OrderServiceErrorMessages.CollectionOfStatusesIsEmpty);

            var httpClient = _clientFactory.CreateClient();

            var response = await httpClient.GetAsync(BuildRequestUrl(orderStatuses));

            return response;
        }

        private string BuildRequestUrl(IEnumerable<OrderStatuses> orderStatuses)
        {
            var baseUrl = $"{_apiSettings.BaseUrl}/{_apiSettings.Orders}?apiKey={_apiSettings.ApiKey}";

            var stringBuilder = new StringBuilder(baseUrl);
            foreach (var orderStatus in orderStatuses)
            {
                stringBuilder.AppendFormat("&statuses={0}", orderStatus.GetEnumValue());
            }

            return stringBuilder.ToString();
        }
    }
}
