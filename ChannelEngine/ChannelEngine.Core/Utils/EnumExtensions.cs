﻿using System;
using System.Runtime.Serialization;

namespace ChannelEngine.Core.Utils
{
    public static class EnumExtensions
    {
        public static string GetEnumValue(this Enum enumVal)
        {
            var attributes = (EnumMemberAttribute[])enumVal
                .GetType()
                .GetField(enumVal.ToString())
                .GetCustomAttributes(typeof(EnumMemberAttribute), false);
            return attributes.Length > 0 ? attributes[0].Value : string.Empty;
        }
    }
}
