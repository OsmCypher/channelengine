﻿namespace ChannelEngine.Core.Infrastructure.ChannelEngine
{
    public class ChannelEngineApiSettings
    {
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
        public string Orders { get; set; }
        public string Products { get; set; }
        public string Offer { get; set; }
    }
}
